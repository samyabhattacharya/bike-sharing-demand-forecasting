# Bike Sharing Demand Forecasting

The project creates several machine learning models for analysis of bike sharing demand in different conditions.
The project includes basic exploratory data analysis, uni-variate and bi-variate analysis, various classification models.
The best classification model produces predicted bike count for any given time point. The plot is shown.
Please change the path according to your folder structure for the train test data in line [66] to rerun the code.
It will take several hours to run the full code.